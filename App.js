import React from 'react';
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native';
import a from './assets/logo.png';
import b from './assets/wifitv.png';
import c from './assets/dering.png';
import d from './assets/pembesar.png';
import e from './assets/akun.jpg';
import f from './assets/ofline.jpg';


const App = ()  => {
  return (
    <View style = {log.back}>
      <View style={log.kratok}>
        <Image source={a} style={log.logo} />
        <Image source={b} style={log.wifitv} />
        <Image source={c} style={log.dering} />
        <Image source={d} style={log.pembesar} />
        <Image source={e} style={log.akun} />
      </View> 
      <ScrollView>
        <Image source={f} style={log.ofline} />
        <Text style={log.teks}>Anda sedang offline</Text>
        <Text style={log.tek}>Tonton video yang didownload, atau</Text>
        <Text style={log.te}>temukan video baru untuk didownload di</Text>
        <Text style={log.t}>koneksi anda</Text>
        <Text style={log.buka}>BUKA DOWNLOAD</Text>
        <Text style={log.co}>COBA LAGI</Text>
      </ScrollView>

    </View>
  );
};

const log = StyleSheet.create ({
  back: {
    backgroundColor: 'white',
    height: 1000,
  },
  kratok: {
    backgroundColor: 'white',
    height: 50,
    flexDirection: 'row',
    shadowColor: 'black',
  },
  logo: {
    width: 90,
    height: 20,
    marginTop: 15,
    marginLeft: 15,
  },
  wifitv: {
    width: 30,
    height: 25,
    marginTop: 15,
    marginLeft: 70,
  },
  dering: {
    width: 18,
    height: 20,
    marginTop: 17,
    marginLeft: 15,
  },
  pembesar: {
    width: 28,
    height: 28,
    marginTop: 14,
    marginLeft: 15,
  },
  akun: {
    width: 28,
    height: 28,
    marginTop: 12,
    marginLeft: 15,
    borderRadius: 20,
  },
  ofline: {
    width: 200,
    height: 200,
    marginTop: 130,
    marginLeft: 15,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 85,
    marginBottom: 20,
  },
  teks: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    paddingLeft: 100,
  },
  tek: {
    fontSize: 14,
    paddingLeft: 70,
   
  },
  te: {
    fontSize: 14,
    paddingLeft: 55,
   
    
  },
  t: {
    fontSize: 14,
    paddingLeft: 145,
    
  },
  buka: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'blue',
    paddingLeft: 129,
    marginTop: 30,
    
  },
  co: {
    fontSize: 14,
    color: 'blue',
    paddingLeft: 155,
    marginTop: 20,
    
  },
});

export default App;